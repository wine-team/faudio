faudio (25.03+dfsg-2) unstable; urgency=medium

  * Switch back to SDL2 to fix initialisation timeouts. Closes:
    #1099537.

 -- Stephen Kitt <skitt@debian.org>  Mon, 10 Mar 2025 23:21:25 +0100

faudio (25.03+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Standards-Version 4.7.2, no change required.

 -- Stephen Kitt <skitt@debian.org>  Sat, 01 Mar 2025 17:21:49 +0100

faudio (25.02+dfsg-1) unstable; urgency=medium

  * New upstream release.

 -- Stephen Kitt <skitt@debian.org>  Sat, 01 Feb 2025 22:35:34 +0100

faudio (24.11+dfsg-1) unstable; urgency=medium

  * New upstream release.

 -- Stephen Kitt <skitt@debian.org>  Fri, 01 Nov 2024 23:15:10 +0100

faudio (24.10+dfsg-1) unstable; urgency=medium

  * New upstream release.

 -- Stephen Kitt <skitt@debian.org>  Thu, 03 Oct 2024 09:05:00 +0200

faudio (24.06+dfsg-1) unstable; urgency=medium

  * New upstream release.

 -- Stephen Kitt <skitt@debian.org>  Sat, 01 Jun 2024 18:23:45 +0200

faudio (24.05+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Standards-Version 4.7.0, no change required.

 -- Stephen Kitt <skitt@debian.org>  Wed, 01 May 2024 19:04:16 +0200

faudio (24.04+dfsg-1) unstable; urgency=medium

  * New upstream release.

 -- Stephen Kitt <skitt@debian.org>  Mon, 01 Apr 2024 18:01:28 +0200

faudio (24.03+dfsg-1) unstable; urgency=medium

  * New upstream release.

 -- Stephen Kitt <skitt@debian.org>  Wed, 06 Mar 2024 07:04:10 +0100

faudio (24.02+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Adjust excluded files for changes in 24.01 and 24.02.

 -- Stephen Kitt <skitt@debian.org>  Sat, 17 Feb 2024 16:09:59 +0100

faudio (23.11+dfsg-1) unstable; urgency=medium

  * New upstream release.

 -- Stephen Kitt <skitt@debian.org>  Wed, 01 Nov 2023 16:04:15 +0100

faudio (23.08+dfsg-1) unstable; urgency=medium

  * New upstream release.

 -- Stephen Kitt <skitt@debian.org>  Fri, 18 Aug 2023 17:04:24 +0200

faudio (23.07+dfsg-1) unstable; urgency=medium

  * New upstream release.

 -- Stephen Kitt <skitt@debian.org>  Sat, 08 Jul 2023 17:43:00 +0200

faudio (23.06+dfsg-1) unstable; urgency=medium

  * New upstream release.

 -- Stephen Kitt <skitt@debian.org>  Sat, 17 Jun 2023 20:15:20 +0200

faudio (23.02+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Standards-Version 4.6.2, no change required.

 -- Stephen Kitt <skitt@debian.org>  Wed, 01 Feb 2023 21:25:22 +0100

faudio (23.01+dfsg-1) unstable; urgency=medium

  * Update file exclusions.
  * New upstream release.

 -- Stephen Kitt <skitt@debian.org>  Sun, 01 Jan 2023 21:31:52 +0100

faudio (22.09.01+dfsg-1) unstable; urgency=medium

  * New upstream release.

 -- Stephen Kitt <skitt@debian.org>  Sun, 04 Sep 2022 18:33:47 +0200

faudio (22.09+dfsg-1) unstable; urgency=medium

  * Add a +dfsg suffix to signal the fact that we repack the source
    code.
  * New upstream release.

 -- Stephen Kitt <skitt@debian.org>  Thu, 01 Sep 2022 17:37:24 +0200

faudio (22.08-1) unstable; urgency=medium

  * New upstream release.

 -- Stephen Kitt <skitt@debian.org>  Wed, 10 Aug 2022 21:22:44 +0200

faudio (22.07-1) unstable; urgency=medium

  * New upstream release.

 -- Stephen Kitt <skitt@debian.org>  Fri, 01 Jul 2022 18:44:55 +0200

faudio (22.06-1) unstable; urgency=medium

  * New upstream release.
  * Standards-Version 4.6.1, no change required.

 -- Stephen Kitt <skitt@debian.org>  Sat, 18 Jun 2022 13:42:54 +0200

faudio (22.05-1) unstable; urgency=medium

  * New upstream release.

 -- Stephen Kitt <skitt@debian.org>  Sat, 07 May 2022 23:08:01 +0200

faudio (22.04-1) unstable; urgency=medium

  * New upstream release.
  * Document Debian patches that aren’t relevant for upstream.
  * Install cmake files in the -dev package.

 -- Stephen Kitt <skitt@debian.org>  Sun, 03 Apr 2022 19:23:23 +0200

faudio (22.03-1) unstable; urgency=medium

  * New upstream release.

 -- Stephen Kitt <skitt@debian.org>  Sat, 05 Mar 2022 15:21:05 +0100

faudio (22.02-1) unstable; urgency=medium

  * New upstream release, dropping gstreamer support.

 -- Stephen Kitt <skitt@debian.org>  Tue, 01 Feb 2022 17:39:01 +0100

faudio (22.01-1) unstable; urgency=medium

  * New upstream release.

 -- Stephen Kitt <skitt@debian.org>  Sat, 01 Jan 2022 18:00:30 +0100

faudio (21.12-1) unstable; urgency=medium

  * New upstream release.

 -- Stephen Kitt <skitt@debian.org>  Sat, 04 Dec 2021 09:46:38 +0100

faudio (21.11-1) unstable; urgency=medium

  * New upstream release.

 -- Stephen Kitt <skitt@debian.org>  Mon, 01 Nov 2021 17:28:33 +0100

faudio (21.10-1) unstable; urgency=medium

  * New upstream release.
  * Recommend gstreamer1.0-libav. Closes: #986440.

 -- Stephen Kitt <skitt@debian.org>  Tue, 26 Oct 2021 21:04:30 +0200

faudio (21.09-1) unstable; urgency=medium

  * New upstream release.
  * Standards-Version 4.6.0, no change required.

 -- Stephen Kitt <skitt@debian.org>  Sun, 05 Sep 2021 17:35:39 +0200

faudio (21.07-1) experimental; urgency=medium

  [ Jens Reyer ]
  * Remove myself from Uploaders.

  [ Stephen Kitt ]
  * New upstream release.

 -- Stephen Kitt <skitt@debian.org>  Sun, 11 Jul 2021 20:00:43 +0200

faudio (21.02-1) unstable; urgency=medium

  * New upstream release.
  * Standards-Version 4.5.1, no change required.

 -- Stephen Kitt <skitt@debian.org>  Tue, 02 Feb 2021 20:20:36 +0100

faudio (21.01-1) unstable; urgency=medium

  * New upstream release.

 -- Stephen Kitt <skitt@debian.org>  Sat, 02 Jan 2021 14:54:17 +0100

faudio (20.12-1) unstable; urgency=medium

  * New upstream release.
  * Update the instructions for importing new versions.

 -- Stephen Kitt <skitt@debian.org>  Thu, 03 Dec 2020 15:09:37 +0100

faudio (20.11-1) unstable; urgency=medium

  * New upstream release, switching from ffmpeg to gstreamer.
  * Add myself to Uploaders.
  * Switch to debhelper compatibility level 13.

 -- Stephen Kitt <skitt@debian.org>  Thu, 05 Nov 2020 21:13:27 +0100

faudio (20.04-2) unstable; urgency=medium

  * Eliminate cmake build percent output.

 -- Michael Gilbert <mgilbert@debian.org>  Sat, 04 Apr 2020 15:11:27 +0000

faudio (20.04-1) unstable; urgency=medium

  * New upstream release (closes: #955500).
  * Exclude cmake files from the binary packages.
  * Detect files missing from the binary packages.
  * Update standards version to 4.5.0 (no changes required).

 -- Michael Gilbert <mgilbert@debian.org>  Fri, 03 Apr 2020 01:46:34 +0000

faudio (19.12-1) unstable; urgency=medium

  * Fix debian/watch.
  * Drop obsolete lintian override 'shlib-calls-exit'.
  * Add README.source.
  * Add Vcs-* fields for new salsa.d.o git repository.
  * Refresh patches.

 -- Jens Reyer <jre.winesim@gmail.com>  Sun, 08 Dec 2019 17:00:40 +0100

faudio (19.11-1) unstable; urgency=medium

  * Update to debhelper 12.
  * Update standards version to 4.4.0.
  * Include Jens Reyer as an uploader.
  * New upstream release (closes: #940203).

 -- Michael Gilbert <mgilbert@debian.org>  Sat, 09 Nov 2019 02:47:48 +0000

faudio (19.07-1) unstable; urgency=medium

  * New upstream release.
  * Build with large file support.
  * Update standards version to 4.4.0.
  * Specify Build-Depends-Package in the symbols file.

 -- Michael Gilbert <mgilbert@debian.org>  Wed, 17 Jul 2019 00:52:33 +0000

faudio (19.06.07-3) unstable; urgency=medium

  * Fix autopkgtest regression introduced by the previous upload.

 -- Michael Gilbert <mgilbert@debian.org>  Fri, 12 Jul 2019 00:57:39 +0000

faudio (19.06.07-2) unstable; urgency=medium

  * Update standards version to 4.4.0.
  * Install tests to multiarch directories (closes: #930466).

 -- Michael Gilbert <mgilbert@debian.org>  Sun, 09 Jun 2019 04:35:04 +0000

faudio (19.06.07-1) unstable; urgency=medium

  * New upstream release.
  * Enable support for the xWMA codec.
  * Build tests and add them to autopkgtest.
  * Fix name of the shared object (closes: #927451).
  * Add cmake to the build dependencies (closes: #926825).
  * List files from Sean Barrett in debian/copyright (closes: #926578).

 -- Michael Gilbert <mgilbert@debian.org>  Sun, 09 Jun 2019 00:49:52 +0000

faudio (19.02-1) unstable; urgency=medium

  * Initial release.

 -- Michael Gilbert <mgilbert@debian.org>  Tue, 26 Feb 2019 20:53:57 -0500
